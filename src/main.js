// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'roboto-fontface/css/roboto/roboto-fontface.css'

import '../src/assets/style.css' // dodatkowe style 

import '../src/assets/maps/cssmap-poland/cssmap-poland.css'  // styl do mapy Polski 
import '../src/assets/maps/cssmap-poland/jquery.cssmap.min.js'  // styl do mapy Polski 

import 'vue-awesome/icons'

import Datetime from 'vue-datetime'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'


Vue.use(Datetime)


Vue.use(require('vue-faker'), {
	locale: 'pl'
});
Vue.use(BootstrapVue);
Vue.config.productionTip = false

new Vue({
	el: '#app',
	router,
	components: {
		App
	},
	template: '<App/>'
})