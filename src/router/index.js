import Vue from 'vue'
import Router from 'vue-router'
import Administratorzy from '@/components/Administratorzy'
import Inspektorzy from '@/components/Inspektorzy'
import Bledy from '@/components/Bledy'
import Statystyki from '@/components/Statystyki'
import Logi from '@/components/Logi'
import About from '@/components/About'

Vue.use(Router)

export default new Router({
  routes: [
  	{ path: '/', component: Administratorzy, name: 'administratorzy'},
  	{ path: '/inspektorzy', component: Inspektorzy, name: 'inspektorzy' }, 	
  	{ path: '/bledy', component: Bledy, name: 'bledy' }, 	
  	{ path: '/statystyki', component: Statystyki, name: 'statystyki' },
  	{ path: '/logi', component: Logi, name: 'logi' },  	
  	{ path: '/o-systemie', component: About, name: 'about' } 	
  ]
})
